"--------------------------------------------------------------------------------------------------------------
" 一般设置（设置编码等）
" -------------------------------------------------------------------------------------------------------------
set helplang=cn
set history=100
set magic
set number
set ruler
set hlsearch
set incsearch
set showmode
set showcmd
set nocompatible
set showmatch
set encoding=utf-8
set fileencodings=utf-8,gb2312,gbk,gb1830,cp936
set fileencoding=utf-8
set fileformat=unix
language messages zh_CN.utf-8

" 取消备份
set nobk 

" 设置颜色主题
color molokai
set t_Co=256

" 隐藏工具栏,菜单栏等
if has("gui")
	set guioptions-=e
	set guioptions-=m
	set guioptions-=T
endif

" 设置字体
if has("gui_running")
  if has("gui_gtk2")
    set guifont=Inconsolata\ 10
  elseif has("gui_win32")
    set guifont=Consolas:h10:cANSI
  endif
endif

" 状态栏设置
set statusline=%f[%{&ff},%{&fenc}]%=%l,%c\ \ \ %P
set backspace=eol,start,indent

set mouse=a
set selection=exclusive
set selectmode=mouse,key

set foldmethod=indent
set foldenable
set foldmethod=manual

" 代码折叠快捷键
nnoremap <space> @=((foldclosed(line('.')) < 0) ? 'zc' : 'zo')<CR>

" markdown 语法设置
augroup mkd
    autocmd BufNewFile,BufRead *.mkd set ai formatoptions=tcronqn2 comments=n:>
    autocmd BufNewFile,BufRead *.md set ai formatoptions=tcronqn2 comments=n:>
augroup END
au BufRead,BufNewFile *.css set ft=css syntax=css3 

" 语法高亮
syntax on
filetype on
filetype plugin on
filetype plugin indent on

" 缩进(用4空格替换tab)
set autoindent shiftwidth=4
set cindent shiftwidth=4
set smartindent
set tabstop=4
set expandtab


"--------------------------------------------------------------------------------------------------------------
" 快捷键设置
"--------------------------------------------------------------------------------------------------------------

" 自定义快捷键起始按键
let mapleader = ','
let g:mapleader = ','

" 重新定义esc按键为kj
inoremap kj <Esc>
" 取消F1
noremap <F1> <Esc>"
" 开启/关闭行号显示
nnoremap <F2> :set nonumber! number?<CR>
" 开启/关闭回车空格tab符号显示
nnoremap <F3> :set list! list?<CR>
" nnoremap <F4> :set wrap! wrap?<CR> QQ冲突了
" 开启/关闭语法高亮
nnoremap <F6> :exec exists('syntax_on') ? 'syn off' : 'syn on'<CR>

" 全选
map <Leader>sa ggVG"

" 取消方向键功能 强迫使用 hjkl
map <Left> <Nop>
map <Right> <Nop>
map <Up> <Nop>
map <Down> <Nop>

" 分屏窗口移动
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

"--------------------------------------------------------------------------------------------------------------
" 插件配置 
"--------------------------------------------------------------------------------------------------------------

" CtrlP 插件 查找文件 缓存 标签等的插件
let g:ctrlp_map = '<Leader>p'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
"set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe  " Windows
"let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(exe|so|dll)$',
  \ 'link': 'some_bad_symbolic_links',
  \ }
"let g:ctrlp_user_command = 'find %s -type f'        " MacOSX/Linux
let g:ctrlp_user_command = 'dir %s /-n /b /s /a-d'  " Windows


" Tabular 代码对齐插件
" :Tab /=  命令对齐等号，其他同理

if exists(":Tabularize")
    nmap <Leader>f= :Tabularize /=<CR>
    vmap <Leader>f= :Tabularize /=<CR>
    nmap <Leader>f: :Tabularize /:\zs<CR>
    vmap <Leader>f: :Tabularize /:\zs<CR>
endif

" mru 查看最近编辑文件插件
"let MRU_Exclude_Files = '^/tmp/.*\|^/var/tmp/.*'  " For Unix
let MRU_Exclude_Files = '^c:\\temp\\.*'           " For MS-Windows
let MRU_Window_Height = 15
"let MRU_Use_Current_Window = 1
let MRU_Auto_Close = 1
map <C-h> :MRU<CR>

" vim-expand-region 智能选择插件
" <s-+> or <s-->


" UltiSnips 代码片段插件
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
" 定义存放代码片段的文件夹 .vim/snippets下，使用自定义和默认的，将会的到全局，有冲突的会提示
le g:UltiSnipsSnippetDirectories=["snippets", "UltiSnips"]

" matchit %号查找配对插件

" vimim 中文快捷输入[打出拼音按(c-6)] 全英文马码

" vim-multiple-cursors 多点编辑插件
let g:multi_cursor_next_key='<C-n>'
let g:multi_cursor_prev_key='<C-p>'
let g:multi_cursor_skip_key='<C-x>'
let g:multi_cursor_quit_key='<Esc>'

